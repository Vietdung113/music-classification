# music-classification

Music-classification

# convert mp3 to image

chỉnh sửa  path trong file audio2image để chạy file audio2image.py

 ```python
 python audio2image.py
 ```
 Chuyển toàn bộ ảnh về  dàng 
 + label/images1.jpg
 + label/image2.jpg 
 

 để load ảnh dùng dataloader pytorch
```python 
python create_data.py
```
                        
# Train Test Split

Chia train dataset thành 2 folder theo đúng tỉ lệ mỗi class của  folder đấy
+ Train folder : 90% . 
+ validation folder : 10% 

```python
python train_test_split.py
```
# Training 
```python 
python train.py
```
# Tính accuracy
chaỵ file để tạo kết quả predict
```
python gather_predict.py
```
tính acuracy trên toàn bộ tập mp3 chạy
```
python predict_audio.py
```

# Error Analysis và Confusion matrix 

```
jupyter notebook
```
chạy từng cell trong file Error Analysis.ipynb
 
 