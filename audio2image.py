import librosa
import pandas as pd
from tqdm import *
import matplotlib.pyplot as plt
import numpy as np
from multiprocessing import Pool, cpu_count
import os.path
import argparse
import shutil

SIZE = 448


class my_element:
    def __init__(self, filename_, class_id_, folder_):
        self.file_name = filename_
        self.class_id = class_id_
        self.folder = folder_


def audio2image_trainset(element):
    audio_path = 'datasets/all_fold/' + element.folder + '/' + element.file_name
    if os.path.isfile(audio_path) == True: 
        samples, sample_rate = librosa.load(audio_path)
        if element.class_id == 1 or element.class_id == 9 or element.class_id == 10:
            print ("________Class 1/9/10")
            for i in range(0, 102, 2):
                sub_sample = samples[sample_rate * i:sample_rate * (i + 20)]

                img_path_melspectrogram = 'datasets/all_fold/images/' + element.folder + '/' + element.file_name.split('.')[0] + '_melspectrogram_%dsec.jpg' % i
                spectrogram = librosa.feature.melspectrogram(sub_sample, sr=sample_rate, n_mels=SIZE)
                log_S = librosa.power_to_db(spectrogram, ref=np.max)
                fig1 = plt.figure(frameon=False)
                fig1.set_size_inches(1, 1)
                ax1 = plt.Axes(fig1, [0, 0, 1, 1], frame_on=False)
                ax1.set_axis_off()
                fig1.add_axes(ax1)
                ax1.imshow(log_S, origin='lower', aspect="auto")
                fig1.savefig(img_path_melspectrogram, dpi=SIZE)
                plt.close()
        else:
            for i in range(0, 108, 8):
                print ("Other Classes")
                sub_sample = samples[sample_rate * i:sample_rate * (i + 20)]
                img_path_melspectrogram = 'datasets/all_fold/images/' + element.folder + '/' + element.file_name.split('.')[0] + '_melspectrogram_%dsec.jpg' % i
                spectrogram = librosa.feature.melspectrogram(sub_sample, sr=sample_rate, n_mels=SIZE)
                log_S = librosa.power_to_db(spectrogram, ref=np.max)
                fig1 = plt.figure(frameon=False)
                fig1.set_size_inches(1, 1)
                ax1 = plt.Axes(fig1, [0, 0, 1, 1], frame_on=False)
                ax1.set_axis_off()
                fig1.add_axes(ax1)
                ax1.imshow(log_S, origin='lower', aspect="auto")
                fig1.savefig(img_path_melspectrogram, dpi=SIZE)

                plt.close()
if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--source', default='fold1', help='Name of fold1')
    args = parser.parse_args()

    df1 = pd.read_csv('datasets/train.csv', header=None)
    my_elements = []
    for idx, row in tqdm(df1.iterrows(), total=df1.shape[0]):
        file_name = row[0]
        class_id = row[1]
        tmp = my_element(file_name, class_id, args.source)
        my_elements.append(tmp)
    p1 = Pool(1)
    p1.map(func=audio2image_trainset, iterable=my_elements)
    p1.close()
