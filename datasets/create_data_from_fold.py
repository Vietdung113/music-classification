import os
from shutil import copyfile
from tqdm import tqdm

if __name__ == '__main__':
    with open('train.csv', 'r') as f:
        datas = f.read().split('\n')
        file_names = [data.split(',')[0] for data in datas[:-1]]
        labels = [data.split(',')[1] for data in datas[:-1]]

    label = list(set([data.split(',')[1] for data in datas[:-1]]))
    for i in label:
        if not os.path.exists(os.path.join(os.path.dirname(__file__), 'all_fold/train_set', i)):
            os.makedirs(os.path.join(os.path.dirname(__file__), 'all_fold/train_set', i))
        if not os.path.exists(os.path.join(os.path.dirname(__file__), 'all_fold/validate_set', i)):
            os.makedirs(os.path.join(os.path.dirname(__file__), 'all_fold/validate_set', i))

    print ('Processing fold1')
    images_train_path = 'all_fold/images/fold1'
    for image in tqdm(os.listdir(images_train_path)):
        file_name = image.split('_')[0] + '.mp3'
        label = labels[file_names.index(file_name)]
        copyfile(os.path.join(images_train_path, image), os.path.join(os.path.dirname(__file__), 'all_fold/train_set', label,image))
    
    print ('Processing fold2')
    images_train_path = 'all_fold/images/fold2'
    for image in tqdm(os.listdir(images_train_path)):
        file_name = image.split('_')[0] + '.mp3'
        label = labels[file_names.index(file_name)]
        copyfile(os.path.join(images_train_path, image), os.path.join(os.path.dirname(__file__), 'all_fold/train_set', label,image))
    
    print ('Processing fold3')
    images_train_path = 'all_fold/images/fold3'
    for image in tqdm(os.listdir(images_train_path)):
        file_name = image.split('_')[0] + '.mp3'
        label = labels[file_names.index(file_name)]
        copyfile(os.path.join(images_train_path, image), os.path.join(os.path.dirname(__file__), 'all_fold/train_set', label,image))
    
    print ('Processing fold4')
    images_train_path = 'all_fold/images/fold4'
    for image in tqdm(os.listdir(images_train_path)):
        file_name = image.split('_')[0] + '.mp3'
        label = labels[file_names.index(file_name)]
        copyfile(os.path.join(images_train_path, image), os.path.join(os.path.dirname(__file__), 'all_fold/train_set', label,image))
    
    print ('Processing fold5')
    images_train_path = 'all_fold/images/fold5'
    for image in tqdm(os.listdir(images_train_path)):
        file_name = image.split('_')[0] + '.mp3'
        label = labels[file_names.index(file_name)]
        copyfile(os.path.join(images_train_path, image), os.path.join(os.path.dirname(__file__), 'all_fold/validate_set', label,image))

