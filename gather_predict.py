import os
import argparse
import pickle
from torchvision import transforms, datasets
from model.network import ResidualAttentionModel_56
import torch
from PIL import Image
from torch.autograd import Variable
import numpy as np
import torch.nn as nn
from tqdm import tqdm

if __name__ =='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', default='/data/dungdv/music/fold_data/music/fold5/',help='path to
                        folder contain evaluate images')
    parser.add_argument('--output',default='./data/predict.txt',help='Path to save file')
    parser.add_argument('--checkpoints',default='./checkpoint/music_attention.pth'm help='Path to
                        checkpoints')
    args = parser.parse_args()
    fout = open(args.output,'w')


    transform = transforms.Compose([
            transforms.ToTensor()
    ])
    model = ResidualAttentionModel_56().cuda()
    model.load_state_dict(torch.load(args.checkpoints))
    model.eval()

    for i, images in tqdm(enumerate(os.listdir(args.data))):
        image = Image.open(os.path.join(args.data,images))
        x = transform(image).float()
        with torch.no_grad():
            x = Variable(x).cuda()
        # create fake batch
        x = x.unsqueeze(0)
        output = model(x)
        # convert to score
        m = nn.Softmax()
        score = m(output).cpu().detach().numpy()[0]
        images = images.split('_')[0]+'.mp3'
        fout.write(os.path.join(args.data,images))
        for i in score:
            fout.write(' ')
            fout.write(str(i))
        fout.write('\n')
    print('oki')
