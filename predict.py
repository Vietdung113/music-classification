import argparse
import os
import torch
from PIL import Image
from torch.autograd import Variable
import torchvision.transforms as transforms
import torch.nn as nn
import numpy as np
from torchvision import datasets
from model.network import ResidualAttentionModel_56
import pickle


class Predictor:
    def __init__(self,checkpoint):
        self.model = ResidualAttentionModel_56().cpu()
        self.model.eval()
        self.model.load_state_dict(torch.load(checkpoint,map_location='cpu'))
        with open('class_to_idx.txt', 'rb') as f:
            self.class_to_idx = pickle.load(f)

    def idx_to_label(self,idx):
        for key, value in self.class_to_idx.items():    # for name, age in dictionary.iteritems():
            if value == idx:
                return key
    def predict(self, image):
        transform= transforms.Compose([transforms.Resize((224,224)),transforms.ToTensor()])
        x = transform(image).float()
        with torch.no_grad():
            x = Variable(x)
        x = x.unsqueeze(0)
        output = self.model(x)
        predict = np.argmax(output.detach().numpy())

        return self.idx_to_label(predict)







if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--images',
                        default='/data/dungdv/music/data/fold1/1005290758554877158_melspectrogram_0sec.jpg',
                        help='Path to public test image folder')
    parser.add_argument('--checkpoint', default='./checkpoint/music_attention.pth')
    args = parser.parse_args()
    image  = Image.open(args.images)
    predictor = Predictor(args.checkpoint)
    print(predictor.predict(image))

