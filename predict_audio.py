import os
import argparse
import numpy as np
from pprint import pprint
import pickle

def process_images_name(images_path):
    image_name = images_path.split('/')[-1]
    image_name = image_name.split('_')[0]
    return image_name
def idx_to_label(class_to_idx,idx):
    for key , value in class_to_idx.items():
        if value == idx:
            return key


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data',default = './data/predict.txt')
    parser.add_argument('--gt', default = '/data/dungdv/music/train.csv')
    args = parser.parse_args()
    correct = 0
    with open(args.data,'r') as f:
        datas = f.read().split('\n')
    with open('./class_to_idx.txt', 'rb') as f:
        class_to_idx = pickle.load(f)
    predict = {}
    with open(args.gt,'r') as f:
        gts = f.read().split('\n')
    ground = {}
    for gt in gts[:-1]:
        ground[gt.split(',')[0]] = gt.split(',')[1]

    for count , data in enumerate(datas[:-1]):
        data = data.split(' ')
        image_name = process_images_name(data[0])
        score  = [float(i) for i in data[1:]]
        try:
            predict[image_name].append(score)
        except:
            predict[image_name] = []
            predict[image_name].append(score)
    for i, (key, value) in enumerate(predict.items()):
        y = np.asarray(value)
        y = np.amax(y,axis=0)
        predicted = np.argmax(y)
        if int(ground[key]) == int(idx_to_label(class_to_idx, predicted)):
            correct +=1
    print('ACC : {}'.format(correct/i))
