import argparse
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
from torchvision import transforms, datasets
from model.network import ResidualAttentionModel_56
from tqdm import tqdm

tqdm.monitor_interval = 0 

def test(model, test_loader, criterion):
    model.eval()
    correct = 0
    total = 0
    val_loss = 0
    # clss_total = list(0. for i in range(10))
    for images, labels in tqdm(test_loader):
        with torch.no_grad():
            images = Variable(images.cuda())
            labels = Variable(labels.cuda())
            outputs = model(images)
            val_loss += criterion(outputs, labels)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels.data).sum()
    avg_loss = float(val_loss) / float(total)
    print('Accuracy of the model on the test images: %.4f ' % (100 * float(correct) / total))
    print('Accuracy of the model on the test images: %.2f ' % (float(correct) / total))
    print('Loss of the model at the test time: {}'.format(avg_loss))
    return float(correct) / total, avg_loss


def train(args):
    # data argument and normalizer
    transform = transforms.Compose([
        transforms.ToTensor()
        # transforms.Normalize(mean=[0.485, 0.456, 0.406],
        #                      std=[0.229, 0.224, 0.225])
    ])
    # create dataset for dataloader
    train_dataset = datasets.ImageFolder(root=args.train,
                                         transform=transform)
    test_dataset = datasets.ImageFolder(root=args.validate,
                                        transform=transform)
    # create train loader and test loader
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=1)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=4, shuffle=False, num_workers=1)
    print('CREATE MODEL:')
    # create model , loss, and optimizer
    model = ResidualAttentionModel_56().cuda()
    criterion = nn.CrossEntropyLoss()
    lr = args.lr
    # optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9, nesterov=True, weight_decay=0.0001)
    optimizer = optim.Adadelta(model.parameters(), lr=lr, rho=0.9, weight_decay=0)
    # optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=1e-4, betas=(0.9, 0.9))

    # class_to_idx = train_loader.dataset.class_to_idx
    acc_best = 0
    loss_val = float('inf')

    model_file = args.model_dir
    # load model if is exist
    try:
        model.load_state_dict(torch.load(model_file))
    except:
        print('Model_file is not exist')
    print('TRAINING START: ')
    for epoch in range(args.num_epochs):
        total_loss = 0
        model.train()
        correct = 0
        n_files = 0
        # train phase
        for i, (images, labels) in tqdm(enumerate(train_loader)):
            images = Variable(images.cuda())
            labels = Variable(labels.cuda())
            # forward + backward
            optimizer.zero_grad()
            outputs = model(images)
            loss = criterion(outputs, labels)
            total_loss += loss.item()
            _, model_predict = torch.max(outputs.data,1)
            correct += (model_predict == labels.data).sum()
            n_files += labels.size(0)

            loss.backward()
            optimizer.step()
            if (i + 1) % 400 == 0:
                print("Epoch [%d/%d], Iter [%d/%d] Loss: %.4f" % (
                    epoch + 1, args.num_epochs, i + 1, len(train_loader),
                    total_loss / (i * args.batch_size)))
        print('Accurary on training set : {}'.format(correct / n_files))
        print('evaluate test set:')
        # test phase
        acc, loss_val_epoch = test(model, test_loader, criterion)
        # save model by acc
        if acc > acc_best:
            print('SAVING MODEL')
            acc_best = acc
            torch.save(model.state_dict(), model_file)
        # save model by loss
        # if loss_val > loss_val_epoch:
        #     print('SAVING MODEL')
        #     loss_val = loss_val_epoch
        #     torch.save(model.state_dict(), model_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', default='/data/dungdv/music/fold_data/music/train_set',
                        help='Path to train folder')
    parser.add_argument('--validate',
                        default='/data/dungdv/music/fold_data/music/validate_set',
                        help='Path to validation folder')
    parser.add_argument('--batch_size', default=48, type=int, help='Batch size')
    parser.add_argument('--num_epochs', default=300, type=int, help='Number of epochs')
    parser.add_argument('--lr', default=1.0, type=float, help='Learning rate')
    parser.add_argument('--model_dir',
                        default='./checkpoint/music.pth', help='Path to save model', type=str)
    args = parser.parse_args()
    train(args)
